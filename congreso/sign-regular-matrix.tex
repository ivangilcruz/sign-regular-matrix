\documentclass[spanish]{beamer}

\setbeamertemplate{theorems}[numbered]
\usefonttheme{professionalfonts}
\usetheme{Warsaw}
%\setBeamercovered{transparent}

\input{preamble}
\usepackage{verbatim}
%\input{myMacros}

\newtheorem{lem}{Lema}[section]
\newtheorem{defi}{Definición}[section]
\newtheorem{teo}{Teorema}[section]


\title[Entradas diagonales de $\Phi$]{Entradas diagonales de la matriz combinada de matrices signo regulares de orden $3$}
\date{\today}
\author{
Maximo Santana\\
Elaine Segura\\
Ivan Gil}
\institute{
\textsc{\footnotesize Universidad Aut\'{o}noma de Santo Domingo}\\
\textsc{\footnotesize Facultad de Ciencias}\\
\textsc{\footnotesize Instituto de Matem\'{a}ticas}
}

\begin{document}

\begin{frame}
\maketitle
\end{frame}

\section{Introducción}
\subsection{Preliminares}
\begin{frame}
  \frametitle{Preliminares}

  \begin{defi}[Matriz combinada]
    Sea $\mathbb{K}$ un cuerpo. Una función $\Phi: GL(n, \mathbb{K}) \longrightarrow \mathbb{M}_n$ tal que
    $\Phi(A) = A \circ A^{-T}$ se conoce como matriz combinada de $A$.
    %Sea $A \in \mathbb{M}_n$ tal que $A$ es no singular. Se define la matriz combinada de $A$ como el mapa

    %$$\Phi(A) = A \circ A^{-T}$$
  \end{defi}

  \begin{teo}\label{teo:inv}
    Si $X, Y \in \mathbb{M}_n$ son matrices diagonales, entonces $\Phi(XAY) = \Phi(A)$.
  \end{teo}

\end{frame}

\begin{frame}
  \begin{defi}[Signatura]
    Una signatura $\epsilon = (\epsilon_k)$ es una secuencia de numeros reales tal que $|\epsilon_k| = 1$.
  \end{defi}

  \begin{defi}[Matriz signo regular]
    Una matriz $A$ de orden $n$ se dice ser {\it signo regular} con signatura $\epsilon = (\epsilon_k)$
    si para $i \leq k \leq n$ y $\alpha, \beta \in Q_{k,n}$ se tiene

    $$\epsilon_k det A[\alpha, \beta] > 0$$

    donde $A[\alpha, \beta] = (a_{ij})$, $i \in \alpha$, $j \in \beta$.
  \end{defi}
\end{frame}

\subsection{Objetivo}
\begin{frame}
\frametitle{Objetivo}
  Caracterizar las entradas diagonales de la matriz combinada de una matriz signo regular de orden $3$.
\end{frame}

\section{Motivación}
\subsection{$\mathbb{M}_1$}
\begin{frame}
\frametitle{Caso 1: $\mathbb{M}_1$}

Este caso es trivial, ya que la matriz combinada de cualquier matriz de orden 1 es la identidad. Asi que no hay nada que caracterizar
sobre sus entradas diagonales.
\end{frame}

\subsection{$\mathbb{M}_2$}
\begin{frame}
\frametitle{Caso 2: $\mathbb{M}_2$}

Cualquier matriz de orden 2 es diagonalmente equivalente a $$S = \left[
        \begin{matrix}
          1 & p \\
          1 & 1
        \end{matrix}
      \right]
$$
lo cual implica, por el teorema $2$, que sus matrices combinadas son iguales. Asi que centraremos el estudio
sobre la combinada de $S$.
\end{frame}

\begin{frame}
Usando la definición, tenemos que las entradas diagonales de $\Phi(S)$ vienen dadas por

 $$u_1 = u_2 = \frac{1}{1-p}$$

Considerando cada una de las signaturas de $S$, tenemos:
\begin{enumerate}
  \item<1-> $\epsilon = (1,1) \Longrightarrow p \in (0,1) \Longrightarrow u_i > 1$
  \item<2-> $\epsilon = (1,-1) \Longrightarrow p > 1 \Longrightarrow u_i < 0$
  \item<3-> $\epsilon = (-1,1) \Longrightarrow p \in (-1, 0)  \Longrightarrow u_i < 1$
  \item<4-> $\epsilon = (-1,-1) \Longrightarrow p < -1  \Longrightarrow u_i > 0$
\end{enumerate}
\end{frame}

\begin{frame}
\frametitle{Resultado}

\begin{teo}
Las condiciones necesarias y suficientes para que dos números reales $u_1$ y $u_2$ sean las entradas
diagonales de la matriz combinada de una matriz $(\epsilon_1,\epsilon_2)$-signo regular de orden $2$ son $\epsilon_1\epsilon_2 u_i > \epsilon_1\frac{1+\epsilon_2}{2}$ y

$$u_1 = u_2$$
\end{teo}

\end{frame}

\section{Matrices signo-regulares de orden 3}
\subsection{Caracterización de las entradas}
\begin{frame}
\frametitle{Signatura $(1, \epsilon_2, \epsilon_3)$}

Cualquier matriz de orden 3 es diagonalmente equivalente a

$$S_1 = \left[
        \begin{matrix}
          r & 1 & p\\
          1 & 1 & 1\\
          q & 1 & s
        \end{matrix}
      \right]
$$

lo cual implica, por el teorema $\ref{teo:inv}$, que sus matrices combinadas son iguales. Asi que centraremos el estudio
sobre la combinada de $S_1$.
\end{frame}

\begin{frame}
  \frametitle{Diagonal de $\Phi(S_1)$}
  Las entradas diagonales de $\Phi(S_1)$ son:
  \begin{enumerate}
    \item $\displaystyle u_1 = \frac{r(s-1)}{p + q -p q + r s - r - s}$
    \item $\displaystyle u_2 = \frac{r s - p q}{p + q -p q + r s - r - s}$
    \item $\displaystyle u_3 = \frac{s(r - 1)}{p + q -p q + r s - r - s}$
  \end{enumerate}
\end{frame}

\begin{frame}
Asumiendo que $S_1$ es $(1, \epsilon_2, \epsilon_3)$-signo regular, tenemos:

\begin{itemize}
  \only<1>{\item<1-> $\epsilon = (1,1,1) \Longrightarrow p,q < 1$ y $r,s > 1$,
  $$p + q -p q + r s - r - s > 0$$

  Por tanto,

  $$u_i > 0, \quad u_1+u_3-u_2-1 < 0$$
  }

  \only<2>{\item<2-> $\epsilon = (1,-1,1) \Longrightarrow p,q > 1$ y $r,s < 1$,
  $$p + q -p q + r s - r - s > 0$$

  Por tanto,

  $$u_i < 0, \quad u_1+u_3-u_2-1 > 0$$
  }
  \only<3>{\item<2-> $\epsilon = (1,1,-1) \Longrightarrow p,q < 1$ y $r,s > 1$,
  $$p + q -p q + r s - r - s < 0$$

  Por tanto,

  $$u_i < 0, \quad u_1+u_3-u_2-1 > 0$$
  }
  \only<4>{\item<2-> $\epsilon = (1,-1,-1) \Longrightarrow p,q > 1$ y $r,s < 1$,
  $$p + q -p q + r s - r - s < 0$$

  Por tanto,

  $$u_i > 0, \quad u_1+u_3-u_2-1 < 0$$
  }
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Signatura $(-1, \epsilon_2, \epsilon_3)$}

Cualquier matriz de orden 3 es diagonalmente equivalente a

$$S_2 = \left[
        \begin{matrix}
          -r & -1 & -p\\
          -1 & -1 & -1\\
          -q & -1 & -s
        \end{matrix}
      \right]
$$

lo cual implica, por el teorema $\ref{teo:inv}$, que sus matrices combinadas son iguales. Asi que centraremos el estudio
sobre la combinada de $S_2$.
\end{frame}

\begin{frame}
  \frametitle{Diagonal de $\Phi(S_2)$}
  Las entradas diagonales de $\Phi(S_2)$ son:
  \begin{enumerate}
    \item $\displaystyle u_1 = \frac{r(1-s)}{r + s - r s + p q - p - q}$
    \item $\displaystyle u_2 = \frac{pq - rs}{r + s - r s + p q - p - q}$
    \item $\displaystyle u_3 = \frac{s(1 - r)}{r + s - r s + p q - p - q}$
  \end{enumerate}
\end{frame}

\begin{frame}
Asumiendo que $S_2$ es $(-1, \epsilon_2, \epsilon_3)$-signo regular, tenemos:

\begin{itemize}
  \only<1>{\item<1-> $\epsilon = (-1,-1,-1) \Longrightarrow p,q > 1$ y $r,s < 1$,
  $$r + s - r s + p q - p - q< 0$$

  Por tanto,

  $$u_i < 0, \quad u_1+u_3-u_2-1 > 0$$
  }

  \only<2>{\item<2-> $\epsilon = (-1,1,-1) \Longrightarrow p,q < 1$ y $r,s > 1$,
  $$r + s - r s + p q - p - q< 0$$

  Por tanto,

  $$u_i > 0, \quad u_1+u_3-u_2-1 < 0$$
  }
  \only<3>{\item<2-> $\epsilon = (-1,-1,1) \Longrightarrow p,q > 1$ y $r,s < 1$,
  $$r + s - r s + p q - p - q > 0$$

  Por tanto,

  $$u_i > 0, \quad u_1+u_3-u_2-1 < 0$$
  }
  \only<4>{\item<2-> $\epsilon = (-1,1,1) \Longrightarrow p,q < 1$ y $r,s > 1$,
  $$r + s - r s + p q - p - q > 0$$

  Por tanto,

  $$u_i < 0, \quad u_1+u_3-u_2-1 > 0$$
  }
\end{itemize}
\end{frame}


\subsection{Resultados preliminares}
\begin{frame}
\frametitle{Resultados preliminares}
  \begin{lem}
    Sean $\epsilon, \tau \in \mathbb{R}$ tales que $|\epsilon| = |\tau| = 1$ y $v \in \mathbb{R}$. Entonces,

    $$(\epsilon = 1 \Rightarrow \tau v > \tau \wedge \epsilon = -1 \Rightarrow \tau v < 0) \Leftrightarrow \epsilon \tau v > \tau\frac{1+\epsilon}{2}$$
  \end{lem}

  \begin{lem}
    Sea $\epsilon \in \mathbb{R}$ tal que $|\epsilon| = 1$ y $v \in \mathbb{R}$. Entonces,

    $$(\epsilon = 1 \Rightarrow p \in (0,1) \wedge \epsilon = -1 \Rightarrow p > 1) \Leftrightarrow \epsilon p < \epsilon, p > 0$$
  \end{lem}
\end{frame}

\begin{frame}
  \begin{lem}\label{lem:e1}
    Una matriz de orden $3$ es $(1,\epsilon_2,\epsilon_3)$-signo regular si, y solo si, esta
    es equivalente, por matrices diagonales positivas, a la matriz

    $$S = \left[
            \begin{matrix}
              \frac{v_{1}}{v_{1} - 1} & 1 & p\\
              1 & 1 & 1\\
              q & 1 & \frac{v_{3}}{v_{3} - 1}
            \end{matrix}
          \right]
    $$

    donde

    \begin{equation}
      \epsilon_{2}v_i > \frac{1+\epsilon_2}{2}, \quad i=1,3
    \end{equation}

    \begin{equation}
      p, q > 0, \quad \epsilon_{2}p < \epsilon_2, \quad \epsilon_{2}q < \epsilon_2
    \end{equation}

    \begin{equation}
      \epsilon_3(1-p)(1-q)(v_1-1)(v_3-1) < \epsilon_3
    \end{equation}
  \end{lem}
\end{frame}

\begin{frame}
  \begin{lem}
    Una matriz de orden $3$ es $(-1,\epsilon_2,\epsilon_3)$-signo regular si, y solo si, esta
    es equivalente, por matrices diagonales positivas, a la matriz

    $$S = \left[
            \begin{matrix}
              \frac{v_{1}}{v_{1} + 1} & -1 & -p\\
              -1 & -1 & -1\\
              -q & -1 & \frac{v_{3}}{v_{3} + 1}
            \end{matrix}
          \right]
    $$

    donde

    \begin{equation}
      \epsilon_{2}v_i < -\frac{1+\epsilon_2}{2}, \quad i=1,3
    \end{equation}

    \begin{equation}
      p, q > 0, \quad \epsilon_{2}p < \epsilon_2, \quad \epsilon_{2}q < \epsilon_2
    \end{equation}

    \begin{equation}
      \epsilon_3(p-1)(q-1)(v_1+1)(v_3+1) > \epsilon_3
    \end{equation}
  \end{lem}
\end{frame}

\section{Resultado}
\subsection{Caracterización de entradas diagonales de $\Phi$}
\begin{frame}
  \begin{teo}
    Las condiciones necesarias y suficientes para que tres números reales $u_1$, $u_2$ y $u_3$ sean las entradas
    diagonales de la matriz combinada de una matriz $(\epsilon_1, \epsilon_2,\epsilon_3)$-signo regular de orden $3$ son $\epsilon_1\epsilon_2\epsilon_3 u_i > 0$ y

    $$\epsilon_1\epsilon_2\epsilon_3(u_1 + u_3 - u_2 - 1) < 0$$
  \end{teo}
\end{frame}

\subsection{Esquema de la demostración}
\begin{frame}
%\frametitle{Signatura $\epsilon = (1, \epsilon_2, \epsilon_3)$}
\frametitle{Suficiencia para la signatura $(1, \epsilon_2, \epsilon_3)$}

  Observe que, por el lema $\ref{lem:e1}$, cualquier matriz $(1, \epsilon_2, \epsilon_3)$-signo regular de orden 3 es diagonalmente equivalente a la matriz

  $$S = \left[
    \begin{matrix}
      \frac{v_{1}}{v_{1} - 1} & 1 & p\\
      1 & 1 & 1\\
      q & 1 & \frac{v_{3}}{v_{3} - 1}
    \end{matrix}
  \right]
$$

donde

\begin{enumerate}
 \item $\epsilon_{2}v_i > \frac{1+\epsilon_2}{2}, \quad i=1,3$
\item  $p, q > 0, \quad \epsilon_{2}p < \epsilon_2, \quad \epsilon_{2}q < \epsilon_2$
\item $\epsilon_3(1-p)(1-q)(v_1-1)(v_3-1) < \epsilon_3$
\end{enumerate}

\end{frame}

\begin{frame}
Si $\Delta = det S$, como $S$ tiene signatura $(1, \epsilon_2, \epsilon_3)$, entonces

$$\epsilon_2 \epsilon_3 u_i = \epsilon_2 \epsilon_3 \frac{s_{ii}S_{ii}}{\Delta} = \frac{s_{ii} \epsilon_2 S_{ii}}{\epsilon_3 \Delta} > 0$$

Por tanto,

$$\epsilon_2 \epsilon_3 u_i > 0$$

\end{frame}

\begin{frame}
Si $s = 1 - (1-p)(1-q)(v_1-1)(v_3-1)$, entonces $ \Delta(v_1-1)(v_3-1)=s$. Asi que, las entradas diagonales de la matriz combinada $\Phi(S)$ son

\begin{itemize}
  \item $\displaystyle u_1 = \frac{v_1}{s}$
  \item $\displaystyle u_2 = \frac{v_1v_3 - pq(v_1-1)(v_3-1)}{s}$
  \item $\displaystyle u_3 = \frac{v_3}{s}$
\end{itemize}
\end{frame}

\begin{frame}
 Entonces

   \begin{eqnarray*}
      u_1 + u_3 - u_2 - 1 &=& \frac{v_1}{s} + \frac{v_3}{s} - \frac{v_1v_3 - pq(v_1-1)(v_3-1)}{s} - \frac{s}{s}\\
        &=& \frac{1}{s}((p(q-1) + q(p-1))(v_1 - 1)(v_3 - 1))
    \end{eqnarray*}

    Asi que, por la segunda y tercera condicion, tenemos

    $$\epsilon_2\epsilon_3(u_1 + u_3 - u_2 - 1) = \frac{\epsilon_2(p(q-1) + q(p-1))(v_1 - 1)(v_3 - 1)}{\epsilon_3s} < 0$$

Por tanto,

    $$\epsilon_2\epsilon_3(u_1 + u_3 - u_2 - 1) < 0$$
\end{frame}

\begin{frame}

\end{frame}

\end{document}
