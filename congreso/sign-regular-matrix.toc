\babel@toc {spanish}{}
\beamer@sectionintoc {1}{Introducci\IeC {\'o}n}{2}{0}{1}
\beamer@subsectionintoc {1}{1}{Preliminares}{2}{0}{1}
\beamer@subsectionintoc {1}{2}{Objetivo}{4}{0}{1}
\beamer@sectionintoc {2}{Motivaci\IeC {\'o}n}{5}{0}{2}
\beamer@subsectionintoc {2}{1}{$\mathbb {M}_1$}{5}{0}{2}
\beamer@subsectionintoc {2}{2}{$\mathbb {M}_2$}{6}{0}{2}
\beamer@sectionintoc {3}{Matrices signo-regulares de orden 3}{12}{0}{3}
\beamer@subsectionintoc {3}{1}{Caracterizaci\IeC {\'o}n de las entradas}{12}{0}{3}
\beamer@subsectionintoc {3}{2}{Resultados preliminares}{24}{0}{3}
\beamer@sectionintoc {4}{Resultado}{27}{0}{4}
\beamer@subsectionintoc {4}{1}{Caracterizaci\IeC {\'o}n de entradas diagonales de $\Phi $}{27}{0}{4}
\beamer@subsectionintoc {4}{2}{Esquema de la demostraci\IeC {\'o}n}{28}{0}{4}
