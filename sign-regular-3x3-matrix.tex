\documentclass[letterpaper, 12pt]{article}
\usepackage{amsmath,amssymb,latexsym,amstext,amsthm,amsfonts,thmtools}
\usepackage{mathtools,mathrsfs}
\usepackage[utf8]{inputenc}
\usepackage{verbatim}
\usepackage{enumerate}
\input{../../macros/math-macros}

\setlength{\oddsidemargin}{-0.5cm}
\setlength{\evensidemargin}{-0.5cm} \setlength{\textwidth}{17.5cm}
\setlength{\textheight}{22cm} \setlength{\topmargin}{-1cm}

\begin{document}
  \title{Diagonal entries of combined matrix in the sign-regular matrix}
  %\author{Maria T. Gass\'o, Ivan Gil, Isabel Gim\'enez,  M\'aximo Santana and Elaine Segura\\
  %  \footnotesize Dpto. de Matem\'atica Aplicada  \\
  %  \footnotesize Universidad Polit\'ecnica de Valencia \\
  %  \footnotesize Spain \\
  %  \footnotesize rbru@mat.upv.es / mgasso@mat.upv.es / igimenez@mat.upv.es/msantana22@uasd.edu.do}
  \author{Ivan Gil Cruz}
  \maketitle %\setlength{\baselineskip}{24pt}
  \setlength{\parindent}{1cm}

\begin{abstract}
  The combined matrix of a nonsingular matrix $A$ is the Hadamard (entry wise) product
  $A \circ \left(A^{-1}\right)^T$.
  It is long standing problem to characterize the range of $\Phi(A)$ if $A$ runs through the
  set of all positive definite $n \times n$ matrices. A partial answer describing the diagonal
  entries of $\Phi(A)$ is the positive definite matrix case given by Fiedler in $1964$.
  Recently in $2011$, Fiedler and Markham in the totally positive case, and Bru R,
  and et in 2016 in the totally negative case, tried to characterize the properties of
  the sequence of the diagonal entries $m_{ij}$ of the combined matrix $\Phi(A)$. Our problem
  is extend this result for sign-regular $3 \times 3$ matrices, so  to find necessary and sufficient
  conditions for the sequence of the diagonal entries of $\Phi(A)$ if $A$ is sign-regular
  matrix $n\times n$ matrices and characterize the range  if $A$ runs through the set of
  all sign-regular  $n\times n$ matrices.
\end{abstract}

\section{Preliminaries}

In this section we introduce fundamental notions for our subject: sign regularity.

\begin{definition}[Signature sequence]
  A signature sequence is real sequence $\epsilon = (\epsilon_i)$ such that $|\epsilon_i| = 1$.
\end{definition}

\begin{definition}[Sign-regular matrix]
  An $n \times m$ matrix A is called sign-regular with signature $\epsilon$ if for
  $\alpha \in Q_{k,n}$ and $\beta \in Q_{k,m}$ with $k = min\{n,m\}$ we have

  $$\epsilon_kA[\alpha,\beta] > 0$$
\end{definition}

\begin{theorem}
An $n \times m$ matrix $A$ is sign-regular with signature $\epsilon$ if $\epsilon_k
A[\alpha,\beta] > 0$ whenever $\alpha \in Q_{k,n}$ and $\beta \in Q_{k,m}$ and $d(\alpha)=d(\beta) =0$,
$k = \min\{n,m\}$.
\end{theorem}

\begin{proof}
  See Ando's article
\end{proof}

\begin{lemma}
Let $\epsilon \in \mathbb{R}$ such that $|\epsilon|=1$ and $v \in \mathbb{R}$. Prove that\\
$$(\epsilon = 1 \Rightarrow v > 1 \wedge \epsilon = -1 \Rightarrow v < 0) \Leftrightarrow \epsilon v > \frac{1+\epsilon}{2}$$
\end{lemma}

\begin{proof}
  \hspace{\fill}
  \begin{enumerate}
    \item[$(\Rightarrow)$] As $\epsilon = 1 \Rightarrow v > 1 \Rightarrow v > \frac{1 + 1}{2} \Rightarrow \epsilon v > \frac{1+\epsilon}{2}, \quad \epsilon = 1.$\\

    Also, $\epsilon = -1 \Rightarrow v < 0  \Rightarrow -v > 0 \Rightarrow -v > \frac{1 - 1}{2} \Rightarrow \epsilon v > \frac{1+\epsilon}{2}, \quad \epsilon = -1.$

    Therefore,

    $$\epsilon v > \frac{1+\epsilon}{2}$$
    \item[$(\Leftarrow)$] If $\epsilon = 1$, we have $\epsilon v > \frac{1+\epsilon}{2} \Rightarrow v > \frac{1+1}{2} = 1 \Rightarrow v > 1$.

    Also, if $\epsilon = -1$, we have $\epsilon v > \frac{1+\epsilon}{2} \Rightarrow -v > \frac{1-1}{2} = 0 \Rightarrow v < 0$.

    Therefore,

    $$\epsilon = 1 \Rightarrow v > 1 \wedge \epsilon = -1 \Rightarrow v < 0$$
  \end{enumerate}
\end{proof}

\begin{lemma}
Let $\epsilon \in \mathbb{R}$ such that $|\epsilon|=1$ and $v \in \mathbb{R}$. Prove that\\
$$(\epsilon = 1 \Rightarrow v < -1 \wedge \epsilon = -1 \Rightarrow v > 0) \Leftrightarrow \epsilon v < -\frac{1+\epsilon}{2}$$
\end{lemma}

\begin{proof}
  \hspace{\fill}
  \begin{enumerate}
    \item[$(\Rightarrow)$] As $\epsilon = 1 \Rightarrow v < -1 \Rightarrow v < -\frac{1 + 1}{2} \Rightarrow \epsilon v < -\frac{1+\epsilon}{2}, \quad \epsilon = 1.$\\

    Also, $\epsilon = -1 \Rightarrow v > 0  \Rightarrow -v < 0 \Rightarrow -v < -\frac{1 - 1}{2} \Rightarrow \epsilon v < -\frac{1+\epsilon}{2}, \quad \epsilon = -1.$

    Therefore,

    $$\epsilon v < -\frac{1+\epsilon}{2}$$
    \item[$(\Leftarrow)$] If $\epsilon = 1$, we have $\epsilon v < -\frac{1+\epsilon}{2} \Rightarrow v < -\frac{1+1}{2} = -1 \Rightarrow v < -1$.

    Also, if $\epsilon = -1$, we have $\epsilon v < -\frac{1+\epsilon}{2} \Rightarrow -v < -\frac{1-1}{2} = 0 \Rightarrow v > 0$.

    Therefore,

    $$\epsilon = 1 \Rightarrow v < -1 \wedge \epsilon = -1 \Rightarrow v > 0$$
  \end{enumerate}
\end{proof}

\begin{lemma}
Let $\epsilon \in \mathbb{R}$ such that $|\epsilon|=1$ and $p \in \mathbb{R}^{+}$. Prove that\\

$$(\epsilon = 1 \Rightarrow p \in (0,1) \wedge \epsilon = -1 \Rightarrow p > 1) \Leftrightarrow \epsilon p < \epsilon, p > 0$$
\end{lemma}

\begin{proof}
  \hspace{\fill}
  \begin{enumerate}
    \item[$(\Rightarrow)$] As $\epsilon = 1 \Rightarrow p \in (0,1) \Rightarrow p < 1, p > 0 \Rightarrow \epsilon p < \epsilon, p > 0, \quad \epsilon = 1.$\\

    Also, $\epsilon = -1 \Rightarrow p > 1  \Rightarrow p > 1, p > 0 \Rightarrow -p < -1, p > 0  \Rightarrow \epsilon p < \epsilon, p > 0, \quad \epsilon = -1.$

    Therefore,

    $$\epsilon p < \epsilon, p > 0$$

    \item[$(\Leftarrow)$] If $\epsilon = 1$, we have $\epsilon p < \epsilon, p > 0 \Rightarrow p < 1, p > 0 \Rightarrow p \in (0,1)$.

    Also, if $\epsilon = -1$, we have $\epsilon p < \epsilon, p > 0 \Rightarrow -p < -1, p > 0  \Rightarrow p > 1, p > 0 \Rightarrow p > 1$.

    Therefore,

    $$\epsilon p < \epsilon, p > 0$$
  \end{enumerate}
\end{proof}

\section{Results}

\input{lem_theo_for_positive_entries}

\include{lem_theo_for_negative_entries}
\end{document}
