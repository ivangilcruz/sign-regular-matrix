%(-1,e2,e3)-signregular matrix
\begin{lemma}
  An $3 \times 3$ matrix is $(-1,\epsilon_2,\epsilon_3)$-sign-regular if and only if it
  is positively diagonally equivalent to the matrix

  $$S = \left[
          \begin{matrix}
            -\frac{v_{1}}{v_{1} + 1} & -1 & -p\\
            -1 & -1 & -1\\
            -q & -1 & -\frac{v_{3}}{v_{3} + 1}
          \end{matrix}
        \right]
  $$

  where

  \begin{equation}
    \epsilon_{2}v_i < -\frac{1+\epsilon_2}{2}, \quad i=1,3 \label{eq:vi2_cond}
  \end{equation}

  \begin{equation}
    p, q > 0, \quad \epsilon_{2}p < \epsilon_2, \quad \epsilon_{2}q < \epsilon_2 \label{eq:pq2_cond}
  \end{equation}

  \begin{equation}
    \epsilon_3(p-1)(q-1)(v_1+1)(v_3+1) > \epsilon_3 \label{eq:det2_cond}
  \end{equation}
\end{lemma}

\begin{proof}
  Let $A$ be an $3 \times 3$ $(-1,\epsilon_2,\epsilon_3)$-sign-regular matrix

  $$A = \left[
      \begin{matrix}
          -a_{11} & -a_{12} & -a_{13}\\
          -a_{21} & -a_{22} & -a_{23}\\
          -a_{31} & -a_{32} & -a_{33}
      \end{matrix}
    \right]
  $$

  We can observe that

  \begin{eqnarray*}
      S &=& \left[
              \begin{matrix}
                -\frac{a_{11} a_{22}}{a_{12} a_{21}} & -1 & -\frac{a_{13} a_{22}}{a_{12} a_{23}}\\
                -1 & -1 & -1\\
                -\frac{a_{22} a_{31}}{a_{21} a_{32}} & -1 & -\frac{a_{22} a_{33}}{a_{23} a_{32}}
              \end{matrix}
            \right]\\
        &=& \left[
            \begin{matrix}
                \frac{1}{a_{12}} & 0 & 0\\
                0 & \frac{1}{a_{22}} & 0\\
                0 & 0 & \frac{1}{a_{32}}
            \end{matrix}
            \right]
            \left[
                \begin{matrix}
                    -a_{11} & -a_{12} & -a_{13}\\
                    -a_{21} & -a_{22} & -a_{23}\\
                    -a_{31} & -a_{32} & -a_{33}
                \end{matrix}
            \right]
            \left[
              \begin{matrix}
                \frac{a_{22}}{a_{21}} & 0 & 0\\
                  0 & 1 & 0\\
                  0 & 0 & \frac{a_{22}}{a_{23}}
              \end{matrix}
            \right]
  \end{eqnarray*}

  the matrix $S$ is positively diagonally equivalent to $A$.\\

  Since $\frac{x}{y} = \frac{v}{v + 1} \Leftrightarrow v = \frac{x}{y-x}$, we can transform $S$ as follows

  \begin{equation*}
    S = \left[
            \begin{matrix}
              -\frac{v_{1}}{v_{1} + 1} & -1 & -p\\
              -1 & -1 & -1\\
              -q & -1 & -\frac{v_{3}}{v_{3} + 1}
            \end{matrix}
          \right]
  \end{equation*}

  where

  $$\displaystyle v_i = \frac{a_{ii} a_{22}}{a_{i2} a_{2i} - a_{ii} a_{22}}, \quad i=1,3$$

  $$\displaystyle p = \frac{a_{13} a_{22}}{a_{12} a_{23}}$$

  $$\displaystyle q = \frac{a_{22} a_{31}}{a_{21} a_{32}}$$

  Now let us see that $p, q, v_1$ and $v_3$ verify \eqref{eq:vi2_cond}, \eqref{eq:pq2_cond} and \eqref{eq:det2_cond}.
  Since $A$ is $(1,\epsilon_2,\epsilon_3)$-sign-regular, we have

  \begin{enumerate}
    \item
          $$\epsilon_2A_{33} > 0 \Leftrightarrow \epsilon_2(a_{11} a_{22} - a_{12} a_{21}) > 0 \Leftrightarrow \epsilon_2v_1 < -\frac{1+\epsilon_2}{2}$$
          $$\epsilon_2A_{11} > 0 \Leftrightarrow \epsilon_2(a_{22} a_{33} - a_{23} a_{32}) > 0 \Leftrightarrow \epsilon_2v_3 < -\frac{1+\epsilon_2}{2}$$
    \item
          $$\epsilon_2A_{31} > 0 \Leftrightarrow \epsilon_2(a_{12} a_{23} - a_{13} a_{22}) > 0 \Leftrightarrow p > 0,  \epsilon_2p < \epsilon_2$$
          $$\epsilon_2A_{13} > 0 \Leftrightarrow \epsilon_2(a_{21} a_{32} - a_{31} a_{22}) > 0 \Leftrightarrow q > 0, \epsilon_2q < \epsilon_2$$
    \item
          $$\epsilon_3\Delta(A) > 0 \Leftrightarrow \epsilon_3\Delta(S) > 0$$

          Hence, $\displaystyle \epsilon_3\Delta(S) = \epsilon_3\frac{(p-1)(q-1)(v_1+1)(v_3+1) - 1}{(v_1+1)(v_3+1)} > 0$

          $\therefore$

          $$\epsilon_3(p-1)(q-1)(v_1+1)(v_3+1) > \epsilon_3$$
  \end{enumerate}

  Hence, $S$ has the required form and satisfied the given conditions.\\

  Now, let $S$ such that

  $$S = \left[
          \begin{matrix}
            -\frac{v_{1}}{v_{1} + 1} & -1 & -p\\
            -1 & -1 & -1\\
            -q & -1 & -\frac{v_{3}}{v_{3} + 1}
          \end{matrix}
        \right]
  $$

  where the conditions \eqref{eq:vi2_cond}, \eqref{eq:pq2_cond} and \eqref{eq:det2_cond} are satisfied.
  We need to prove that any matrix $A$ positively diagonally equivalent to $S$ is a $(-1,\epsilon_{2}, \epsilon_{3})$-sign-regular matrix.\\

  So, we can observe that

  \begin{eqnarray*}
      A &=& \left[
            \begin{matrix}
                x_1 & 0 & 0\\
                0 & x_2 & 0\\
                0 & 0 & x_3
            \end{matrix}
            \right]
            \left[
                \begin{matrix}
                    -\frac{v_{1}}{v_{1} + 1} & -1 & -p\\
                    -1 & -1 & -1\\
                    -q & -1 & -\frac{v_{3}}{v_{3} + 1}
                  \end{matrix}
            \right]
            \left[
              \begin{matrix}
                y_1 & 0 & 0\\
                  0 & y_2 & 0\\
                  0 & 0 & y_3
              \end{matrix}
            \right]\\
        &=& \left[
              \begin{matrix}
                -\frac{v_{1} x_{1} y_{1}}{v_{1} + 1} & -x_{1} y_{2} & -p x_{1} y_{3}\\
                -x_{2} y_{1} & -x_{2} y_{2} & -x_{2} y_{3}\\
                -q x_{3} y_{1} & -x_{3} y_{2} & -\frac{v_{3} x_{3} y_{3}}{v_{3} + 1}
              \end{matrix}
            \right]
  \end{eqnarray*}

  Since by \eqref{eq:vi2_cond} and \eqref{eq:pq2_cond} all entries are
  negative. Now, we only need to show that $\epsilon_2A_{ij}$ and the determinant are positive.\\

  By \eqref{eq:vi2_cond} and \eqref{eq:pq2_cond}, we can observe that

  \begin{eqnarray}
    \epsilon_2A_{11} &=& \epsilon_{2}x_2x_3y_2y_3\left(\frac{v_3}{v_3+1} - 1\right) \nonumber \\
                     &=& -\frac{x_2x_3y_2y_3}{\epsilon_{2}(v_3+1)} > 0
  \end{eqnarray}

  \begin{eqnarray}
    \epsilon_{2}A_{12} &=& \epsilon_{2}x_2x_3y_1y_3\left(\frac{v_3}{v_3 + 1} - q\right)\nonumber \\
          &=& x_2x_3y_1y_3\left(\epsilon_{2}(1- q) - \frac{1}{\epsilon_{2}(v_3 + 1)}\right) > 0 \nonumber
  \end{eqnarray}

  $$\epsilon_{2}A_{13} = \epsilon_{2}x_2x_3y_1y_2(1 - q) > 0$$

  \begin{eqnarray}
    \epsilon_{2}A_{21} &=& \epsilon_{2}x_1x_3y_2y_3\left(\frac{v_3}{v_3 +1} - p\right)\nonumber \\
          &=& x_1x_3y_2y_3\left(\epsilon_{2}(1- p) - \frac{1}{\epsilon_{2}(v_3 +1)}\right) > 0 \nonumber\\
          &\Leftrightarrow& \epsilon_{2}\frac{v_3}{v_3 +1} > \epsilon_{2}p \label{eq:a21_2}
  \end{eqnarray}

  \begin{eqnarray}
    \epsilon_{2}A_{23} &=& \epsilon_{2}x_1x_3y_1y_2\left(\frac{v_1}{v_1 +1} - q\right) \nonumber\\
          &=& x_1x_3y_1y_2\left(\epsilon_{2}(1- q) - \frac{1}{\epsilon_{2}(v_1 +1)}\right) > 0 \nonumber\\
          &\Leftrightarrow& \epsilon_{2}\frac{v_1}{v_1 + 1} > \epsilon_{2}q \label{eq:a23_2}
  \end{eqnarray}

  $$\epsilon_{2}A_{31} = \epsilon_{2}x_1x_2y_2y_3(1 - p) > 0$$

  \begin{eqnarray}
    \epsilon_{2}A_{32} &=& \epsilon_{2}x_1x_2y_1y_3\left(\frac{v_1}{v_1 + 1} - p\right)\nonumber \\
          &=& x_1x_2y_1y_3\left(\epsilon_{2}(1- p) - \frac{1}{\epsilon_{2}(v_1 + 1)}\right) > 0 \nonumber
  \end{eqnarray}

  $$\epsilon_{2}A_{33} = \epsilon_{2}x_1x_2y_1y_2\left(\frac{v_1}{v_1+1} - 1\right) = -\frac{x_1x_2y_1y_2}{\epsilon_{2}(v_1+1)} > 0$$

  By \eqref{eq:a21_2} and \eqref{eq:a23_2}, we have

  \begin{eqnarray}
    \epsilon_{2}\frac{v_1v_3}{(v_1 + 1)(v_3 + 1)} > \epsilon_{2}pq &\Leftrightarrow& \epsilon_{2}A_{22} = \epsilon_{2}\left(\frac{v_1v_3}{(v_1 + 1)(v_3 + 1)} - pq\right) > 0 \nonumber
  \end{eqnarray}

  By \eqref{eq:det2_cond}, we have

  $$\epsilon_3\Delta(A) > 0 \Leftrightarrow \epsilon_3\Delta(S) > 0$$

  Since $\displaystyle \epsilon_3\Delta(A) = \epsilon_3(x_1x_2x_3y_1y_2y_3)\frac{(p-1)(q-1)(v_1+1)(v_3+1) - 1}{(v_1+1)(v_3+1)} > 0$.\\

  Hence, $A$ is sign-regular with signature $(-1, \epsilon_2, \epsilon_3)$.

\end{proof}

\begin{theorem}
  A necessary and sufficient condition for three numbers, $u_1 ,u_2, u_3$, to be the
  diagonal entries of the combined matrix of an $3 \times 3$ $(-1,\epsilon_2,\epsilon_3)$-sign-regular matrix is that
  $\epsilon_2\epsilon_3u_i < 0$ and

  $$\epsilon_2\epsilon_3(u_1 + u_3 - u_2 - 1) > 0$$
\end{theorem}

\begin{proof}
  \hspace{\fill}
  \begin{enumerate}
    \item[$(\Rightarrow)$] Observe that all $3 \times 3$ $(-1,\epsilon_2,\epsilon_3)$-sign-regular matrix
    is positively diagonally equivalent to the matrix $A$

    $$A = \left[
            \begin{matrix}
              -\frac{v_{1}}{v_{1} + 1} & -1 & -p\\
              -1 & -1 & -1\\
              -q & -1 & -\frac{v_{3}}{v_{3} + 1}
            \end{matrix}
          \right]
    $$

    where

    $$\epsilon_{2}v_i < -\frac{1+\epsilon_2}{2}, \quad i=1,3$$
    $$p, q > 0, \quad \epsilon_{2}p < \epsilon_2, \quad \epsilon_{2}q < \epsilon_2$$
    $$\epsilon_3(p-1)(q-1)(v_1+1)(v_3+1) > \epsilon_3$$

    Since $\displaystyle \Delta = \frac{(p-1)(q-1)(v_1+1)(v_3+1) - 1}{(v_1+1)(v_3+1)}$.\\
    If $\displaystyle s = (p-1)(q-1)(v_1+1)(v_3+1) -1$, then $\displaystyle \frac{1}{\Delta} = \frac{(v_1+1)(v_3+1)}{s}$.

    Now, the diagonals entries of combined matrix $\Phi(A)$ are

    \begin{itemize}
      \item $\displaystyle u_1 = \frac{v_1}{s} \Rightarrow \epsilon_2\epsilon_3u_1 = \frac{\epsilon_2v_1}{\epsilon_3s} < 0$
      \item $\displaystyle u_1 = \frac{pq(v_1+1)(v_3+1) - v_1v_3}{s} \Rightarrow \epsilon_2\epsilon_3u_2 = \frac{\epsilon_2[pq(v_1+1)(v_3+1) - v_1v_3]}{\epsilon_3s} < 0$
      \item $\displaystyle u_3 = \frac{v_3}{s} \Rightarrow \epsilon_2\epsilon_3u_3 = \frac{\epsilon_2v_3}{\epsilon_3s} < 0$
    \end{itemize}

    To finish our proof, we have

    \begin{eqnarray*}
      u_1 + u_3 - u_2 - 1 &=& \frac{v_1}{s} + \frac{v_3}{s} - \frac{pq(v_1+1)(v_3+1) - v_1v_3}{s} - \frac{s}{s}\\
        &=& \frac{1}{s}(v_1 + v_3 + v_1v_3 - pq(v_1 + 1)(v_3 + 1) - s)\\
        &=& \frac{1}{s}(v_1 + v_3 + v_1v_3 + (-pq - (p-1)(q-1))(v_1 + 1)(v_3 + 1) + 1)\\
        &=& \frac{1}{s}((p(1-q) + q(1-q))(v_1 + 1)(v_3 + 1))
    \end{eqnarray*}

    Hence,
      $$\epsilon_2\epsilon_3(u_1 + u_3 - u_2 - 1) = \frac{\epsilon_2(p(1-q) + q(1-q))(v_1 + 1)(v_3 + 1)}{\epsilon_3s} > 0$$

    Therefore,

    $$\epsilon_2\epsilon_3(u_1 + u_3 - u_2 - 1) > 0$$

    \item[$(\Leftarrow)$]
  \end{enumerate}

    To do
\end{proof}
